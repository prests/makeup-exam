import random
import sys
import PollardRho

def findB(num, T): #T = (T^2)mod num for i=1 to k-1
    for i in range(1,k):
        T = (T**2)%num
        if(T==1):
            return 1 #Composite
        if(T==-1):
            return -1 #Prime
    return 6 #Something went wrong

def findT(num, a , m): #T = (a^m)mod num
    return ((long(a)**m)%num)

def findKM(num, k, m): #n-1 = m*(a^k)
    newM = float(num)/(2**k)
    if(not newM.is_integer()):
        return k-1, int(m)
    else:
        return findKM(num, (k+1), newM)

if __name__ == '__main__':
    sys.setrecursionlimit(sys.maxint) #Extend the recursion limit
    num = int(raw_input("Input Number: "))
    k, m = findKM(num-1, 0, float(num-1))
    print('K: %s M: %s' %(k,m))
    if(k==1):
        T = findT(num, 2, m)
        if(T==num-1):
            print('%s is a prime number' %(num))
        else:
            factor1, factor2 = PollardRho.main(num)
            if(factor1 == -1):
                print('%s is a prime number' %(num))
            else:
                print('%s is a composite number' %(num))
                print('%s has factors %s and %s' %(num,factor1,factor2))
    elif(k==0):
        print('%s is a composite number' %(num))
        factor1, factor2 = PollardRho.main(num)
        print('%s has factors %s and %s' %(num,factor1,factor2))
    else:
        a = random.randint(2,num-2)
        T = findT(num, 2, m)
        b = findB(num, T)
        if(b == 1):
            factor1, factor2 = PollardRho.main(num)
            if(factor1 == -1):
                print('%s is a prime number' %(num))
            else:
                print('%s is a composite number' %(num))
                print('%s has factors %s and %s' %(num,factor1,factor2))
        else:
            print(str(num) + ' is a prime number')