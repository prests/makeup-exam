def gcd(x, y): #GCD using Euclidean Algorithm
    while(y):
        x, y = y, x%y
    return x

def fnc(val, num): #f(x) = x^2 + 1 mod n
    return (((val**2)+1)%num)

def main(num):
    x, y = (2,)*2 #starting value 2
    d = 1 #placeholder
    while d == 1: #Sequence (once gcd isn't 1)
        x = fnc(x, num)
        y = fnc(fnc(y, num), num)
        d = gcd(abs(x-y),num)
    if d == num: #It doesn't have a gcd greater than 1
        return -1, -1 #return failure
    else:
        return d, (num/d) #return gcd and n/gcd

if __name__ == '__main__':
    num = int(raw_input("Input Number: "))
    factor1, factor2 = main(num)
    print(factor1)
    print(factor2)
