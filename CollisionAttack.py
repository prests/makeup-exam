import hash
import random

def randomBinString(lenM): #Creates a random message with length same as message
    temp = ''
    for i in range(0, lenM):
        temp = temp + (str(random.randint(0,1)))
    return temp

def findCollision(m, h, mDict):
    for i in range(0, (2**(len(m)/2))):
        randomMessage = ''
        while(True):#Keeps creating random bit string until a unique one is generated
            temp = randomBinString(len(m)) #Random bit string with same length as message
            if(not (temp in mDict)):
                randomMessage = temp
                break #Unique message
        mDict[randomMessage] = hash.main(randomMessage) #Assign hash value
        #print('Random Message: %s Hash Message: %s' %(randomMessage, hash.main(randomMessage)))
         
    collision = False
    for i in mDict:
        if((mDict[i] == h) and  i != m): #If hash value of message = hash value of a random message, collision found
            print('Found a collision: %s!' %(i))
            collision = True
    if(not collision): #No collision found so try again
        print('No collision found. Dictionary size: %s' %(len(mDict)))
        findCollision(m, h, mDict) #save dictionary so you don't repeat tries


if __name__ == '__main__':
    m = raw_input('Enter message (bits): ')
    h = hash.main(m)
    mDict = {} #key: message value: hash value
    findCollision(m, h, mDict)