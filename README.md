# Makeup Exam

> Question 1:

The way I solved question one is by creaing a birthday paradox attack and this is how it works:

1. Generate a message and it's respected hash
2. Generate 2^(length(message)/2) random messages and their respected hashes
3. check if their hashes are the same as the message hash
4. if they are you found a collision if not repeat starting at step 2

This information can be used later to figure out how the algorithm works by forcing hashes

> Question 2:

Textbooks problems see pdf

> Question 3:

Answers to question see pdf code also attached

Interesting thing to note the Miller Rabin algorithm didn't like when the power got really long so it would just say it's composite, my solution was to back check with Pollard Rho to see if valid