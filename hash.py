def hash(m, hValue):
    hash = []
    for i in range(0, len(m)):
        hash.append(int(m[i]) ^ int(hValue[i % len(hValue)]))
    return hash

def main(m):
    hValue = '1010110011'
    h = hash(m, hValue)
    hstr = ''
    for i in h:
        hstr = hstr + str(i)
    return hstr